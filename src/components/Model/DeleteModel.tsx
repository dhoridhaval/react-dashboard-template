import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import CloseIcon from '@mui/icons-material/Close';

const DeleteModel = (props) => {
  return (
    <div>
      <Dialog open={props.open} onClose={props.handleClose}>
        <div style={{ minWidth: '450px' }}>
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
              marginRight: '20px'
            }}
          >
            <DialogTitle id="alert-dialog-title">Confirmation</DialogTitle>
            <CloseIcon onClick={props.handleClose} />
          </div>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              {props.message}
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button variant="contained" onClick={props.handleClose}>
              No
            </Button>
            <Button
              variant="contained"
              color="error"
              onClick={props.handleClose}
              autoFocus
            >
              Yes
            </Button>
          </DialogActions>
        </div>
      </Dialog>
    </div>
  );
};

export default DeleteModel;
