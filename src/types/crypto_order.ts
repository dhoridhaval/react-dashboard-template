export type CryptoOrderStatus = 'completed' | 'pending' | 'failed';

export interface CryptoOrder {
  id: string;
  status: CryptoOrderStatus;
  userName: string;
  userEmail: string;
  address: string;
  mobileNo: number;
  created: string;
}
