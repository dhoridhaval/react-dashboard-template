import ReactDOM from 'react-dom';
import { HelmetProvider } from 'react-helmet-async';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { SidebarProvider } from 'src/contexts/SidebarContext';
import App from 'src/App';
import * as serviceWorker from 'src/serviceWorker';
import store from './store';
import 'nprogress/nprogress.css';

ReactDOM.render(
  <Provider store={store}>
    <HelmetProvider>
      <SidebarProvider>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </SidebarProvider>
    </HelmetProvider>,
  </Provider>,
  document.getElementById('root')
);

serviceWorker.unregister();
