import { Grid, Container } from '@mui/material';
import { TextField } from '@material-ui/core';
import Button from '@mui/material/Button';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';

const AddUsers = () => {
  let navigate = useNavigate();
  const [user, setUser] = useState({
    username: '',
    email: '',
    name: '',
    phone: '',
    website: ''
  });

  const { name, email, username, phone, website } = user;
  const onInputChange = (e) => {
    setUser({ ...user, [e.target.name]: e.target.value });
  };
  const onSubmit = async (e) => {
    e.preventDefault();
    await axios.post('http://localhost:3001/users', user);
    navigate('/management/users-list');
  };
  return (
    <>
      <Container maxWidth="lg">
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignItems="stretch"
          spacing={3}
        >
          <Grid item xs={12} margin="20px">
            <h3>Fill users admin details :</h3>
            <br />
            <br />
            <form onSubmit={(e) => onSubmit(e)}>
              <Grid container spacing={2} marginY="30px">
                <Grid
                  xs={6}
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    gap: '10px',
                    paddingRight: '30px'
                  }}
                >
                  <div>Users Name</div>
                  <TextField
                    type="text"
                    name="name"
                    placeholder="Enter Your Name"
                    value={name}
                    onChange={(e) => onInputChange(e)}
                    variant="outlined"
                    fullWidth
                  />
                </Grid>
                <Grid
                  xs={6}
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    gap: '10px'
                  }}
                >
                  <div>Users Email</div>
                  <TextField
                    label="Email"
                    name="email"
                    value={email}
                    onChange={(e) => onInputChange(e)}
                    variant="outlined"
                    fullWidth
                  />
                </Grid>
              </Grid>
              <Grid container spacing={2} marginY="30px">
                <Grid
                  xs={6}
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    gap: '10px',
                    paddingRight: '30px'
                  }}
                >
                  <div>Users Phone</div>
                  <TextField
                    label="Phone"
                    name="phone"
                    value={phone}
                    onChange={(e) => onInputChange(e)}
                    variant="outlined"
                    fullWidth
                  />
                </Grid>
                <Grid
                  xs={6}
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    gap: '10px'
                  }}
                >
                  <div>Users-Name</div>
                  <TextField
                    id="outlined-basic"
                    label="Username"
                    name="username"
                    value={username}
                    onChange={(e) => onInputChange(e)}
                    variant="outlined"
                    fullWidth
                  />
                </Grid>
                <Grid
                  xs={6}
                  style={{
                    display: 'flex',
                    marginTop: '30px',
                    alignItems: 'center',
                    gap: '10px'
                  }}
                >
                  <div>Users Website</div>
                  <TextField
                    id="outlined-basic"
                    label="Website"
                    name="website"
                    value={website}
                    onChange={(e) => onInputChange(e)}
                    variant="outlined"
                    fullWidth
                  />
                </Grid>
              </Grid>

              <button style={{ marginRight: '20px',padding: '10px'  }}>Add User</button>
            </form>
          </Grid>
        </Grid>
      </Container>
    </>
  );
};

export default AddUsers;
