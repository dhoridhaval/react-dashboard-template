import { Card } from '@mui/material';
import { CryptoOrder } from 'src/types/crypto_order';
import UserListTable from './UserListTable';

const RecentOrders = () => {
  const userListOrders: CryptoOrder[] = [
    {
      id: '1',
      userName: 'Dhaval Dhori',
      status: 'completed',
      userEmail: 'dhoridhaval310@gamil.com',
      address: '1000 Park Ave',
      mobileNo: 9099482001,
      created: '01-11-2022'
    },
    {
      id: '2',
      userName: 'Keyur H',
      status: 'completed',
      userEmail: 'keyur@gmail.com',
      address: '1001 Park Avenue',
      mobileNo: 9099482002,
      created: '03-11-2022'
    },
    {
      id: '3',
      userName: 'Mayur patel',
      status: 'failed',
      userEmail: 'mayur423@gmail.com',
      address: '1003 St. Nicholas Ave.',
      mobileNo: 9099482003,
      created: '08-11-2022'
    },
    {
      id: '4',
      userName: 'Jiyan Patel',
      status: 'completed',
      userEmail: 'jiyanpatel@gmail.com',
      address: '100 Bleecker St',
      mobileNo: 9099482004,
      created: '15-11-2022'
    },
    {
      id: '5',
      userName: 'Kapil a',
      status: 'pending',
      userEmail: 'kapila@gmail.com',
      address: '100 E31st St',
      mobileNo: 9099482005,
      created: '19-11-2022'
    },
    {
      id: '6',
      userName: 'Shailesh',
      status: 'completed',
      userEmail: 'shailesh@gmail.com',
      address: '100 East 68th Street Apt. 14E',
      mobileNo: 9099482006,
      created: '21-11-2022'
    },
    {
      id: '7',
      userName: 'Rohit ambaliya',
      status: 'pending',
      userEmail: 'rohitambaliya@gmail.com',
      address: '100 Jane St.',
      mobileNo: 9099482007,
      created: '21-11-2022'
    },
    {
      id: '8',
      userName: 'Meet soni',
      status: 'completed',
      userEmail: 'meetsoni@gmail.com',
      address: '100 Jane St PH-J',
      mobileNo: 9099482008,
      created: '21-11-2022'
    },
    {
      id: '9',
      userName: 'Manish Pandey',
      status: 'completed',
      userEmail: 'manishpandey@gmail.com',
      address: '100 John St. Apt 2208',
      mobileNo: 9099482009,
      created: '21-11-2022'
    },
    {
      id: '10',
      userName: 'Tarjen',
      status: 'failed',
      userEmail: 'tarjen@gmail.com',
      address: '100 East 68th Street Apt. 14E',
      mobileNo: 9099482010,
      created: '21-11-2022'
    }
  ];
  const getUsers = async () => {
    const payload = {};
    try {
      const data = await getUsers();
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Card>
      <UserListTable userListOrders={userListOrders} />
    </Card>
  );
};

export default RecentOrders;
