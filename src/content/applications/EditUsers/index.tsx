import { Helmet } from 'react-helmet';
import { Grid, Container } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { Box, Button, TextField } from '@material-ui/core';
import { useNavigate, useParams } from 'react-router-dom';
import axios from 'axios';

const EditUsers = () => {
  let navigate = useNavigate();
  const { id } = useParams();
  const [user, setUser] = useState({
    username: '',
    email: '',
    name: '',
    phone: '',
    website: ''
  });

  const { name, email, username, phone, website } = user;
  const onInputChange = (e) => {
    setUser({ ...user, [e.target.name]: e.target.value });
  };

  useEffect(() => {
    loadUser();
  }, []);

  const onSubmit = async (e) => {
    e.preventDefault();
    await axios.put(`http://localhost:3001/users/${id}`, user);
    navigate('/management/users-list');
  };

  const loadUser = async () => {
    const result = await axios.get(`http://localhost:3001/users/${id}`);
    setUser(result.data);
  };

  return (
    <>
      <Container maxWidth="lg">
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignItems="stretch"
          spacing={3}
        >
          <Grid item xs={12} margin="20px">
            <h3>Edit details :</h3>
            <br />
            <br />
            <form onSubmit={(e) => onSubmit(e)}>
              <Grid container spacing={2} marginY="30px">
                <Grid
                  xs={6}
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    gap: '10px',
                    paddingRight: '30px'
                  }}
                >
                  <div>Users Name</div>
                  <TextField
                    type="text"
                    name="name"
                    placeholder="Enter Your Name"
                    value={name}
                    onChange={(e) => onInputChange(e)}
                    variant="outlined"
                    fullWidth
                  />
                </Grid>
                <Grid
                  xs={6}
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    gap: '10px'
                  }}
                >
                  <div>Users Email</div>
                  <TextField
                    label="Email"
                    name="email"
                    value={email}
                    onChange={(e) => onInputChange(e)}
                    variant="outlined"
                    fullWidth
                  />
                </Grid>
              </Grid>
              <Grid container spacing={2} marginY="30px">
                <Grid
                  xs={6}
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    gap: '10px',
                    paddingRight: '30px'
                  }}
                >
                  <div>Users Phone</div>
                  <TextField
                    label="Phone"
                    name="phone"
                    value={phone}
                    onChange={(e) => onInputChange(e)}
                    variant="outlined"
                    fullWidth
                  />
                </Grid>
                <Grid
                  xs={6}
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                    gap: '10px'
                  }}
                >
                  <div>Users-Name</div>
                  <TextField
                    id="outlined-basic"
                    label="Username"
                    name="username"
                    value={username}
                    onChange={(e) => onInputChange(e)}
                    variant="outlined"
                    fullWidth
                  />
                </Grid>
                <Grid
                  xs={6}
                  style={{
                    marginTop:"30px",
                    display: 'flex',
                    alignItems: 'center',
                    gap: '10px'
                  }}
                >
                  <div> Users Website</div>
                  <TextField
                    id="outlined-basic"
                    label="Website"
                    name="website"
                    value={website}
                    onChange={(e) => onInputChange(e)}
                    variant="outlined"
                    fullWidth
                  />
                </Grid>
              </Grid>

              <button
                style={{ marginRight: '20px', padding: '10px' }}
              >
                Update User
              </button>
              <button style={{padding: '10px'}}>Cancel</button>
            </form>
          </Grid>
        </Grid>
      </Container>
    </>
  );
};

export default EditUsers;
