import { Box, Container, Card, Button, TextField } from '@mui/material';
import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';

type Loginprops = {
  email?: string;
  password?: string;
};

const useStyles = makeStyles((theme) => ({
  form: {
    width: '100%',
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

const Login = () => {
  const initialValues = { email: '', password: '' };
  const [formValues, setFormValues] = useState(initialValues);
  const [formErrors, setFormErrors] = useState<Loginprops>({});
  const [isSubmit, setIsSubmit] = useState(false);

  const classes = useStyles();
  // let dispatch = useDispatch();

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormValues({ ...formValues, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setFormErrors(validate(formValues));
    setIsSubmit(true);
  };

  const validate = (values) => {
    const errors: Loginprops = {};
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;

    if (!values.email) {
      errors.email = 'Email is required!';
    } else if (!regex.test(values.email)) {
      errors.email = 'This is not a valid email format!';
    }
    if (!values.password) {
      errors.password = 'Password is required';
    } else if (values.password.length < 4) {
      errors.password = 'Password must be more than 4 characters';
    } else if (values.password.length > 10) {
      errors.password = 'Password cannot exceed more than 10 characters';
    } 
    {
      if (!values.password){
        errors.password="password is required"
      }
    }
   
    
    return errors;
  }; 
  return (
    <Container maxWidth="sm">
      <Box
        display="flex"
        justifyContent="center"
        alignItems="center"
        height="100vh"
      >
        <Card sx={{ p: 4, mb: 10, borderRadius: 2 }}>
          <Box
            display="flex"
            justifyContent="center"
            py={2}
            alignItems="center"
            flexDirection="column"
            height="full"
          >
            <h2>Login</h2>
            <form className={classes.form} noValidate onSubmit={handleSubmit}>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                autoFocus
                id="email"
                label="E-mail"
                type="text"
                name="email"
                placeholder="Enter your E-mail"
                value={formValues.email}
                onChange={handleChange}
              />
              <span style={{ color: 'red' }}>{formErrors.email}</span>
              <TextField
                margin="normal"
                required
                fullWidth
                name="password"
                id="password"
                autoComplete="current-password"
                label="password"
                type="password"
                placeholder="Enter your password"
                value={formValues.password}
                onChange={handleChange}
              />
              <span style={{ color: 'red' }}>{formErrors.password}</span>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
              >
                Login
              </Button>
            </form>
          </Box>
        </Card>
      </Box>
    </Container>
  );
};

export default Login;
